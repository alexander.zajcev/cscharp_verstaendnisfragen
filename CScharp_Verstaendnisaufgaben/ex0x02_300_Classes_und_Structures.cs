﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x02_300_Classes_und_Structures : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } = 
            "Demonstrieren, " +
            "wie unterschiedlich sich der Reference-Typ (Class) und der Value-Typ (Structure) verhalten\n" +
            "a) Beim Zuweisen einer Variable des Typs an eine andere.\n" +
            "b) Wenn man die Typen beim Aufruf von Funktionen als Parameter uebergibt.\n" +
            "- Video: 0x02.2 25:35";

        public override void Execute()
        {
            Point_C pc01 = new Point_C { x = 10, y = 20 };
            Point_C pc02 = pc01;
            pc02.x = 1000;
            pc02.y = 2000;
            Console.WriteLine("pc01: " + pc01);
            Console.WriteLine("pc02: " + pc02);
            Console.WriteLine("pc02 und pc01 Variablen sind Aliase auf dieselbe Instanz.");
            Console.WriteLine();

            Point_S ps01 = new Point_S { x = 30, y = 40 };
            Point_S ps02 = ps01;
            ps02.x = 3000;
            ps02.y = 4000;
            Console.WriteLine("ps01: " + ps01);
            Console.WriteLine("ps02: " + ps02);
            Console.WriteLine("ps02 und ps01 Variablen bezeichnen unterschiedliche Instanzen.");
            Console.WriteLine();

            inc_C(pc01);
            inc_S(ps01);
            Console.WriteLine("pc01: " + pc01);
            Console.WriteLine("ps01: " + ps01);
            Console.WriteLine("Class-Parameters werden an die Funktionen by Reference uebergeben und die Struct-Parameters by Value.");
        }

        public void inc_C(Point_C val)
        {
            val.x += 1;
            val.y += 1;
        }

        public void inc_S(Point_S val)
        {
            val.x += 1;
            val.y += 1;
        }

    }

    class Point_C {
        public int x, y;
        public override string ToString()
        {
            return $"x={x}; y={y} [class]";
        }
    }

    struct Point_S
    {
        public int x, y;
        public override string ToString()
        {
            return $"x={x}; y={y} [struct]";
        }
    }
}
