﻿using System;
using System.Collections.Generic;
using System.Diagnostics.Metrics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    public class ex0x10_100_Mutex : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Zwei Threads ändern gleichzeitig denselben globalen counter.\n" +
            "Jeder Thread macht insgesamt dieselbe Anzahl von Änderungen.\n" +
            "Ein Thread inkrementiert um 1 und der andere dekrementiert um 1.\n" +
            "Aufgabe: Threadsafety gewaehrleisten mit dem Mutex so, \n" +
            "dass am ende der Counter so groß ist wie am Anfang, und zwar 0.\n" +
            "- Video 0x0a ab 19:40\n" +
            "- code: 0x0a B_Mutex\n";

        static volatile int counter1 = 0;
        static volatile int counter2 = 0;
        static int anzahlAenderungen = 1000000;

        static Mutex global_mutex = new Mutex(); //Moeglichkeit 1: mit Mutex
        static object global_lock = new object(); //Moeglichkeit 2: mit lock

        public override void Execute()
        {
            Thread t1 = new Thread(increment);
            Thread t2 = new Thread(decrement);

            t1.Start();
            t2.Start();

            t1.Join();
            t2.Join();

            Console.WriteLine($"Results: counter1 = {counter1}, counter2 = {counter2}\n");

        }

        public void increment()
        {
            for (int i = 0; i < anzahlAenderungen; i++)
            {
                //global_mutex.WaitOne(); //Moeglichkeit 1: mit Mutex 
                counter1 += 1;
                //global_mutex.ReleaseMutex();

                //lock (global_lock) //Moeglichkeit 2: mit Lock
                //{
                    counter2 += 1;
                //}
            }
        }
        public void decrement()
        {
            for (int i = 0; i < anzahlAenderungen; i++)
            {
                //global_mutex.WaitOne(); //Moeglichkeit 1: mit Mutex 
                counter1 -= 1;
                //global_mutex.ReleaseMutex();

                //lock (global_lock) //Moeglichkeit 2: mit Lock
                //{
                    counter2 -= 1;
                //}
            }
        }

    }
}
