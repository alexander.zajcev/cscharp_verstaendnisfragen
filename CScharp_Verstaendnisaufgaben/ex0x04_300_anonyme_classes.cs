﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x04_300_anonyme_classes : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) eine Variable \"p1\" des variablen Typs deklarieren. \n" +
            "Initialisieren sie mit einer Instanz eines anonymen Typs, " +
            "der Zwei Attribute hat: ein int-Attribut mit dem Namen intAttr und ein string-Attribut mit dem Namen strAttr.\n" +
            "das int-Attribut bei der Initialisierung auf 3 setzen, und das string-Attribut auf \"Hallo\".\n" +
            "- Video 0x04.1 17:00\n" +
            "\n" +
            "2) Eine int-Variable mit dem Namen intVar deklarieren und mit dem Wert 5 initialisieren.\n" +
            "Eine string-Variable mit dem Namen strVar deklarieren und mit dem Wert \"Salut\" initialisieren.\n" +
            "eine Variable \"p2\" des variablen Typs deklarieren und mit Instanz eines anonymen Typs initialisieren.\n" +
            "der anonyme Typ soll zwei Attribute aus den Variablen intVar und strVar bekommen.\n" +
            "";

        public override void Execute()
        {
            // 1)
            var p1 = new { intAttr = 3, strAttr = "Hallo" };
            Console.WriteLine($"p1.intAttr --> {p1.intAttr}, p1.strAttr --> {p1.strAttr}");

            // 2)
            int intVar = 5;
            string strVar = "Salut";
            var p2 = new {intVar, strVar};
            Console.WriteLine($"p2.intVar --> {p2.intVar}, p2.strVar --> {p2.strVar}");

        }
    }
}
