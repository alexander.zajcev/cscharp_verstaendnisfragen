﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http.Headers;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x03_500_generische_typ_parameter : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Eine Klasse \"GenerischerParameter\" implementieren, die ein einzelnes Attribut vom Generischen Typ hat.\n" +
            "Den Konstruktor implementieren, der einen einzelnen Parameter vom Generischen Typ nimmt und das Attribut setzt.\n" +
            "Ueberschreiben die ToString Methode so, dass sie den generischen Attribut ausgibt.\n" +
            "- Video 0x03.1 31:00\n" +
            "\n" +
            "2) Den Typ des generischen Parameters beschraenken so, dass nur diejenigen Typ-Parameter erlaubt sind, " +
            "die das Interface IConvertible implementieren.\n" +
            "eine Methode \"double add5komma5()\" zu der Klasse hinzufuegen, die zu dem Attribut die Zahl 5.5 addiert und das Ergebnis ausgibt.";

        public override void Execute()
        {
            Console.WriteLine($"1) new GenerischerParameter<string>(\"foo bar\") = {new GenerischerParameter<string>("foo bar")}");
            
            Console.WriteLine($"2) new GenerischerParameter<Double>(2.1).add5komma5() = {new GenerischerParameter<Double>(2.1).add5komma5()}");
            Console.WriteLine($"   new GenerischerParameter<Boolean>(true).add5komma5() = {new GenerischerParameter<Boolean>(true).add5komma5()}");

        }

        public class GenerischerParameter<T> where T:IConvertible
        {
            //Aufgabe 1

            public T attribut;

            public GenerischerParameter(T attribut)
            {
                this.attribut = attribut;
            }

            public override string ToString()
            {
                return $"der attribut hat den Wert: {attribut}";
            }

            //Aufgabe 2
            public double add5komma5()
            {
                return attribut.ToDouble(new CultureInfo("")) + 5.5;
            }
        }
    }
}
