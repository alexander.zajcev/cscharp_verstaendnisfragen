﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x04_400_delegates_lambdas : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Mit Hilfe eines Lambda-Ausdrucks eine Funktion mit dem namen \"teileAdurchB\" erstellen. " +
            "Die Fuktion soll zwei double-Parameter nehmen, den ersten Parameter durch den zweiten teilen und das Ergebnis zurueckgeben.\n" +
            "\n" +
            "2) Einen Typ mit dem Namen \"MathFunction\" erstellen, der Referenzen auf Funktionen repraesentiert, " +
            "die einen double-Parameter nehmen und einen double-Wert zurueckgeben.\n" +
            "- Eine Variable \"f\" des Typs MathFunction deklarieren und mit der Referenz auf die Funktion Math.Floor initialisieren.\n" +
            "Ueber die Variable f die refernzierte Funktion mit dem Parameter 5.4 aufrufen und den Rueckgabewert ausgeben.\n" +
            "- Der Variable f eine Referenz auf die Funktion Math.Ceiling zuweisen.\n" +
            "Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.\n" +
            "- Video 0x04.2 vom Anfang\n" +
            "\n" +
            "3)Der Variable f eine Referenz auf eine anonyme Funktion zuweisen, " +
            "die den uebergebenen double-Parameter mit 2 multipliziert und das Ergebnis zurueckgibt.\n" +
            "Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.\n" +
            "-Video 0x04.2 07:00\n" +
            "\n" +
            "4) mithilfe eines Lambda-Ausdrucks zuweisen der Variable f eine Referenz auf eine anonyme Funktion, " +
            "die den uebergebenen double-Parameter durch 2 teilt und das Ergebnis zurueckgibt.\n" +
            "Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.\n" +
            "- Video 0x04.2 07:50\n" +
            "\n" +
            "5.1) Eine Variable mit dem Namen \"a\" eines Anonymous Typs deklarieren.\n" +
            "Ter Typ soll Referenzen auf Funktionen representieren, " +
            "zwei Parameter entgegennehmen: der erste vom Typ \"double\", der zweite vom typ \"int\". " +
            "Und einen double Ergebnis zurueckgeben.\n" +
            "Der Variable \"a\" eine Referenz auf eine anonyme Lambda-Funktion zuweisen. " +
            "Die anonyme Funktion soll den ersten int-Parameter durch den zweiten Teilen und das Ergebnis zuruckegeben.\n" +
            "Ueber die Variable a die refernzierte Funktion mit den Parameter 5 und 3 aufrufen und den Rueckgabewert ausgeben.\n" +
            "\n" +
            "5.2) Eine Variable mit dem Namen \"v\" eines Anonymous Typs deklarieren.\n" +
            "Ter Typ soll Referenzen auf void-Funktionen representieren, die zwei string-Parameter entgegennehmen.\n" +
            "Der Variable \"v\" eine Referenz auf eine anonyme Lambda-Funktion zuweisen. " +
            "Die anonyme Funktion soll die zwei Strings, getrennt durch ein Leerzeiche auf der Console ausgeben.\n" +
            "Ueber die Variable \"v\" die refernzierte Funktion mit den Parameter \"hello\" und \"world\" aufrufen.\n" +
            "- Video 0x04.2 10:45";

        public override void Execute()
        {
            // 1)
            Console.WriteLine($"1) teileAdurchB(5, 2) --> {teileAdurchB(5, 2)}\n");

            // 2)
            MathFunction f = Math.Floor;
            Console.WriteLine($"2)\n{f.Method.Name}: f(5.4) --> {f(5.4)}");

            f = Math.Ceiling;
            Console.WriteLine($"{f.Method.Name}: f(5.4) --> {f(5.4)}\n");

            // 3) 
            f = delegate (double a) { return a * 2; };
            Console.WriteLine($"3) {f.Method.Name}: f(5.4) --> {f(5.4)}\n");

            // 4)
            f = (a) => a / 2;
            Console.WriteLine($"4) {f.Method.Name}: f(5.4) --> {f(5.4)}\n");

            // 5.1)
            Func<double, int, double> a = (dbl, intgr) => dbl / intgr;
            Console.WriteLine($"5.1) {a.Method.Name}: a(5, 3) --> {a(5, 3)}\n");

            // 5.2)
            Action<string, string> v = (str1, str2) => { Console.WriteLine(str1 + " " + str2); };
            Console.WriteLine($"5.2) {v.Method.Name}: v(\"hello\", \"world\") --> \n");
            v("hello", "world");
        }

        // 1)
        public double teileAdurchB(double a, double b) => a / b;

        // 2)
        delegate double MathFunction(double x);
    }
}
