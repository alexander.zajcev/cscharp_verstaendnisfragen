﻿namespace ZA_Verstaendnisaufgaben
{
    public abstract class Verstaendnisaufgabe
    {
        public virtual string Aufgabenstellung { set; get; } = "";

        public virtual void Execute()
        {

        }

        public void ExecuteWithBanner()
        {
            string className = this.GetType().Name;
            Console.WriteLine($"=== {className} ==================================\n");
            Console.WriteLine($"Aufgabe:\n{Aufgabenstellung}\n");
            Console.WriteLine("--- Executing ---\n");
            Execute();
            Console.WriteLine("\n=== The End ===\n\n\n");
        }

    }
}
