﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x10_050_Thread_creation : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Create one Thread, start it and wait before it complets it's job.\n" +
            "- Video 0x0a (von Anfang)\n" +
            "- Threads mit Parameter: Video 17:30";

        public override void Execute()
        {
            ThreadStart threadDelegate = new ThreadStart(DoWork);
            Thread t1 = new Thread(threadDelegate) { Name = "t1"};
            t1.Start();
            t1.Join(); // Join bedeutet: nicht weiter gehen, bevor der Thread t1 seine arbeit erledigt.

            Thread t2 = new Thread(new ThreadStart(DoWork)) { Name = "t2" };
            Thread t3 = new Thread(DoWork) { Name = "t3" }; // Der Compiler macht fuer diese kurze Schreibweise das, was oben steht mit dem ThreadStart.
            Thread t4 = new Thread(() => { Console.WriteLine("Thread from Lambda;\n"); Thread.Sleep(1000);  });

            //threads mit parameter
            int intParam = 33433;
            Thread pt1 = new Thread(() => DoWorkWithParams(intParam, "Hallo")) { Name = "pt1" }; //Parameter mithilfe von Lambda
            pt1.Start();
            pt1.Join();

            My00050WorkerClass w1 = new My00050WorkerClass();
            w1.intParam = 10;
            w1.strParam = "Hallo";

            Thread pt2 = new Thread(w1.DoWork) { Name = "pt2" };
        }

        public void DoWork()
        {
            Console.WriteLine($"Der Thread {Thread.CurrentThread.Name}.DoWork");   
        }

        public void DoWorkWithParams(int intParam, string strParam)
        {
            Console.WriteLine($"Der Thread {Thread.CurrentThread.Name}.DoWorkWithParams intParam: {intParam}; strParam: {strParam}");
        }

    }

    internal class My00050WorkerClass
    {
        public int intParam;
        public string strParam = "";

        public void DoWork()
        {
            Console.WriteLine($"intParam={intParam}, strParam={strParam} \n");
        }
    }
}
