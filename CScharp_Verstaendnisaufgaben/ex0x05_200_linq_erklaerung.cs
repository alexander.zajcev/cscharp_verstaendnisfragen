﻿using System;
using System.Collections.Generic;
//using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x05_200_linq_erklaerung : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Als Vorbereitung drei ganz normale Funktionen implementieren:\n" +
            "- int getStringLength(string s) : gibt die Laenge des Strings zurueck.\n" +
            "- bool obWirDasWortBrauchenOderNicht(string s) : gibt zurueck, ob die Laenge des Strings groesser als 4 ist\n" +
            "- string stringToUpperCase(string s) : gibt den String mit allen Grossbuchstaben zurueck.\n" +
            "- string-Array mit dem Namen \"strOrig\" und zehn Woerter initialisieren: " +
            "eins,zwei,drei,vier,fuenf,sechs,sieben,acht,neun,zehn\n" +
            "\n" +
            "Mithilfe der vom LINQ bereitgestellten Extension-Methoden das Array \"strOrig\" wie folgt transformieren:\n" +
            "1.1) Sortieren absteigend nach der Wortlaenge. Ergebnis in der Variable \"strOrder\" speichern.\n" +
            "1.2) Filtern die in strOrder gespeicherten Woerter so, dass nur die Woerter mit der Laenge groesser 4 bleiben." +
            "Ergebnis speichern in der Variable \"strWhere\".\n" +
            "1.3) Alle in strWhere gespeicherten Woerter zu Grossbuchstaben umwandeln und in der Variabel \"strSelect\" speichern.\n" +
            "\n" +
            "2) Dieselben Transformationen an dem strOrig mit einem einzigen LINQ-Ausdruck durchfuehren " +
            "und in der Variable \"linqResult\" speichern.\n" +
            "\n" +
            "- Video von 13:10 bis 25:45";

        public override void Execute()
        {
            // 1)
            string[] strOrig = { "eins", "zwei", "drei", "vier", "fuenf", "sechs", "sieben", "acht", "neun", "zehn" };
            IEnumerable<string> strOrder = strOrig.OrderByDescending(getStringLength);
            IEnumerable<string> strWhere = strOrder.Where(obWirDasWortBrauchenOderNicht);
            IEnumerable<string> strSelect = strWhere.Select(stringToUpperCase);


            Console.WriteLine("1.1) sortiert nach Laenge:");
            foreach (string str in strOrder)
            {
                Console.WriteLine(str);
            }

            Console.WriteLine("1.2) Laenge 4 ausgefiltert:");
            foreach (string str in strWhere)
            {
                Console.WriteLine(str);
            }

            Console.WriteLine("1.3) Zu Grossbuchstaben konvertiert:");
            foreach (string str in strSelect)
            {
                Console.WriteLine(str);
            }
            Console.WriteLine();

            // 2)
            Console.WriteLine("2) Sortiert, filtered und zu Grossbuchstaben konvertiert mit einem LINQ-Ausdruck:");
            IEnumerable<string> linqResult = 
                from s in strOrig 
                where obWirDasWortBrauchenOderNicht(s)
                orderby getStringLength(s) descending
                select s.ToUpper();
            foreach (string str in linqResult) { Console.WriteLine(str); }

        }

        public int getStringLength(string s)
        {
            return s.Length;
        }

        public bool obWirDasWortBrauchenOderNicht(string s)
        {
            if(s.Length > 4) { return true; }
            else { return false; }
        }

        public string stringToUpperCase(string s)
        {
            return s.ToUpper();
        }
    }
}
