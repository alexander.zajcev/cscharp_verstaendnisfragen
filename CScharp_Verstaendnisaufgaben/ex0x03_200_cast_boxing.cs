﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x03_200_cast_boxing : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Demonstrieren, " +
            "wie passiert Boxing beim Casting von einem Value-Type (z.B. Struct) zu einem Reference-Type (z.B. Interface).\n" +
            "- Video 0x03.1 min 04:30";

        public override void Execute()
        {
            SampleStruct objSampleStruct = new SampleStruct() { Name = "Klaus" };
            I_Namable obj1_I_Namable = objSampleStruct;

            Console.WriteLine(
                $"Beim Zuweisen eines Struct-Object zu einer Interface-Variable" +
                $" wird der Struct-Object geklonnt und in einen Reference-Typ umgewandelt (Boxing):\n" +
                $"objSampleStruct.Name = {objSampleStruct.Name}\n" +
                $"obj1_I_Namable.Name = {obj1_I_Namable.Name}\n\n" +
                $"Als Bestaetigung aendern wir eine der beiden Variablen und sehen, dass die andere unveraendert bleibt:\n");

            obj1_I_Namable.Name = "Hans";

            Console.WriteLine
                ($"objSampleStruct.Name = {objSampleStruct.Name}\n" +
                $"obj1_I_Namable.Name = {obj1_I_Namable.Name}\n\n");

            Console.WriteLine(
                $"Um zu zeigen, dass beim Boxing der Value-Typ (Struct) in einen Reference-Typ gepackt wurde " +
                $"weise ich einer anderen Interface-Variable die erste Interface-Variable zu:\n");

            I_Namable obj2_I_Namable = obj1_I_Namable;

            Console.WriteLine(
                $"obj1_I_Namable.Name = {obj1_I_Namable.Name}\n" +
                $"obj2_I_Namable.Name = {obj2_I_Namable.Name}\n\n" +
                $"Jetzt beim Aendern einer der beiden Variablen die zweite aendert sich auch, was bestaetigt, " +
                $"dass die beide Variablen auf die selbe Instanz zeigen (Reference-Type):\n");

            obj2_I_Namable.Name = "Ralf";

            Console.WriteLine
                ($"obj2_I_Namable.Name = {obj2_I_Namable.Name}\n" +
                $"obj1_I_Namable.Name = {obj1_I_Namable.Name}\n");

        }

    }

    public interface I_Namable
    {
        public string Name { get; set; }    
    }

    public struct SampleStruct : I_Namable
    {
        public string Name { get; set; }   

    }


}
