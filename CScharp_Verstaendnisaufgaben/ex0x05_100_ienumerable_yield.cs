﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Reflection.Metadata.Ecma335;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x05_100_ienumerable_yield : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Eine Klasse mit dem Namen \"ThreeValuesCollection\" implementieren. " +
            "Die Klasse soll einen Konstruktor mit drei int-Parametern besitzen.\n" +
            "Die Klasse soll den Interface \"IEnumerable\" implementieren so, " +
            "dass Instanzen der Klasse in der foreach-Schleife verwendet werden koennen.\n" +
            "Beim Durchlaufen der foreach-Schleife sollen die drei im Konstruktor " +
            "entgegengenommene int-Parametern nacheinander ausgegeben werden\n" +
            "Um \"IEnumerable\" zu implementieren, ist noch eine Klasse erforderlich, " +
            "die den \"IEnumerator\" Interface implementiert. Die Klasse soll \"ThreeValuesEnumerator\" heissen.\n" +
            "\n" +
            "2) Eine Funktion mit dem Namen \"threeValuesFunktion\" schreiben, " +
            "die genau dieselben Anforderungen wie im Punkt 1) erfuellt:\n" +
            "- funktion nimmt 3 int-Parametern entgegen\n" +
            "- der Rueckgabetyp der Funktion ist IEnumerable\n" +
            "- Beim durchlaufen des Rueckgabewertes der Funktion " +
            "in der foreach-Schleife sollen nacheinander die in den Parameter uebergebene Zahlen ausgegeben werden.\n" +
            "- Video 0x05.1 vom Anfang";

        public override void Execute()
        {
            // 1)
            Console.WriteLine($"1) ThreeValuesCollection(5, 7, 9), foreach-Schleife: -->\n");
            ThreeValuesCollection col = new ThreeValuesCollection(5, 7, 9);
            foreach (int i in col)
            {
                Console.WriteLine("naechster Element: " + i);
            }

            // 2)
            Console.WriteLine($"\n2) foreach(int i in threeValuesFunktion(100, 200, 300)) -->\n");
            foreach (int i in threeValuesFunktion(100, 200, 300))
            {
                Console.WriteLine("naechster Element: " + i);
            }

        }

        // 2)
        public IEnumerable threeValuesFunktion(int zahl1, int zahl2, int zahl3)
        {
            yield return zahl1;
            yield return zahl2;
            yield return zahl3;
        }
    }

    // 1)
    public class ThreeValuesCollection : IEnumerable
    {
        private int _zahl1;
        private int _zahl2;
        private int _zahl3;

        public ThreeValuesCollection(int zahl1, int zahl2, int zahl3)
        {
            _zahl1 = zahl1;
            _zahl2 = zahl2;
            _zahl3 = zahl3;
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return new ThreeValuesEnumerator(_zahl1, _zahl2, _zahl3);
        }
    }

    public class ThreeValuesEnumerator : IEnumerator
    {
        private int _zahl1;
        private int _zahl2;
        private int _zahl3;

        private int _currentIndex = -1;

        public ThreeValuesEnumerator(int zahl1, int zahl2, int zahl3)
        {
            _zahl1 = zahl1;
            _zahl2 = zahl2;
            _zahl3 = zahl3;
        }

        object IEnumerator.Current
        {
            get
            {
                if (_currentIndex == 0)
                {
                    return _zahl1;
                }
                else if(_currentIndex == 1)
                {
                    return (_zahl2);
                }
                else
                {
                    return _zahl3;
                }
            }
        }

        public bool MoveNext()
        {
            if(_currentIndex < 2)
            {
                _currentIndex++;
                return true;
            }
            else
            {
                return false;
            }
        }

        public void Reset()
        {
            _currentIndex = -1;
        }
    }
}
