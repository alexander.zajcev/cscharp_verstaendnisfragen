﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x03_300_operatoren : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Implementieren eine Klasse MyCast mit einem Integer-Property intProp0. " +
            "In dieser Klasse implementieren zwei explicite Cast-Operatoren, " +
            "einen von integer zu MyCast und einen von MyCast zu integer\n" +
            "- Video 0x03.1 08:50\n" +
            "\n" +
            "2) In der Klasse MyCast implementieren den \"+\"-Operator mit dem man " +
            "einen MyCast-Object mit einem anderen MyCast-Object oder mit ener int-Variable addieren kann.\n" +
            "- Video 0x03.1 16:50\n" +
            "\n" +
            "3) In die Klasse MyCast hinzufuegen noch ein privates Integer-Property (intProp1).\n" +
            "Implementieren einen Indexer-Operator ([]), der fuer den index [0] das Property intProp0 setzt/zurueckgibt\n" +
            "und fuer alle anderen Indexe setzt/zurueckgibt das Property intProp1\n" +
            "- Video 0x03.1 23:00\n";

        public override void Execute()
        {
            MyCast mc = (MyCast)15;
            Console.WriteLine($"1.1) MyCast mc = (MyCast)15; --> mc = {mc}");
            int i = (int)mc;
            Console.WriteLine($"1.2) int i = (int)mc; --> i = {i}");
            mc = mc + 5;
            Console.WriteLine($"2.1) mc = mc + 5; --> mc = {mc}");
            MyCast mc2 = 10 + mc;
            Console.WriteLine($"2.2) MyCast mc2 = 10 + mc; --> mc2 = {mc2}");
            Console.WriteLine($"2.3) mc + mc2 = --> {mc + mc2}");
            Console.WriteLine($"3) mc[0] = --> {mc[0]}");
            mc[55] = 99;
            Console.WriteLine($"   mc[55] = 99; --> mc[55] = {mc[55]}");
        }
    }

    public class MyCast
    {
        public int intProp0 { get; set; }
        public int intProp1 { get; set; }   

        // Aufgabe 1.1 
        public static explicit operator MyCast(int i)
        {
            MyCast m = new MyCast();
            m.intProp0 = i;
            return m;
        }

        // Aufgabe 1.2
        public static explicit operator int(MyCast mc)
        {
            return mc.intProp0;
        }

        // Aufgabe 2.1
        public static MyCast operator +(MyCast mc1, MyCast mc2)
        {
            MyCast ausgabe = new MyCast();
            ausgabe.intProp0 = mc1.intProp0 + mc2.intProp0;
            return ausgabe;
        }

        // Aufgabe 2.2
        public static MyCast operator +(MyCast mc, int i)
        {
            MyCast ausgabe = new MyCast();
            ausgabe.intProp0 = mc.intProp0 + i;
            return ausgabe;
        }

        // Aufgabe 2.3
        public static MyCast operator +(int i, MyCast mc)
        {
            //hier soll man nicht nochmal das gleiche implementieren,
            //sondern die implementierung von oben verwenden:
            return mc + i;
        }

        // Aufgabe 3
        public int this[int idx] {
            get 
            {
                if(idx == 0) { return intProp0; }
                else { return intProp1; }
            }
            set
            {
                if(idx == 0) { intProp0 = value; }
                else { intProp1 = value; }
            }
        }


        public override string ToString()
        {
            return $"[MyCast Instanz mit intProp0={intProp0}]";
        }
    }
}
