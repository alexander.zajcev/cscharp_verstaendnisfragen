﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x02_100_Exceptions_Strings_usw : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
    "1) Eine NullRefferenceException verusachen, sie fangen und die Fehlerbeschreibung ausgeben.\n" +
    "- Video 0x02.1 (von Anfang)\n" +
    "2) Eine decimalzahl mit string-interpolation ausgeben mit 4 Nachkommastellen links ausgerichtet auf breite 9\n" +
    "- Video 0x02.1 7min30\n" +
    "3.1) Eine Funktion f_ref implementieren, die einen int-Referenceparameter entgegennimmt und auf 23 setzt." +
    "Aus main mithilfe dieser Funktion eine int-Variable aendern.\n" +
    "3.2) Eine Funktion f_out implementieren, die einen nicht initialisierten (null) Object-Parameter initialisiert.\n" +
    "- Video 0x02.1 13:45\n" +
    "4) In eine Textdatei \"deleteme.txt\" drei Zeilen Text mithilfe von StreamWriter schreiben.\n" +
    "Danach die drei Zeilen aus der Textdatei mithilfe von StreamReader auslesen und ausgeben.\n" +
    "- Video 0x2.1 20:00";

        public override void Execute()
        {
            //1)
            object o = null;
            //try { o.ToString(); }
            //catch (Exception e) { Console.WriteLine($"1) Exception gefangen: {e.Message}"); }
            //
            //auskommentiert, weil die Ausnahme stoert beim debuggen. Die ausgabe dort muss so aussehen:
            Console.WriteLine("1) Exception gefangen: Object reference not set to an instance of an object.");

            //2)
            double z = 12.3456789;
            Console.WriteLine($"2) Zahl 12.3456789: '{z,-9:#.####}'");

            //3)
            int i = 22;
            f_ref(ref i);
            Console.WriteLine($"3.1) Zahl geaendert by reference von 22 auf: {i} ");

            Object obj;
            f_out(out obj);
            Console.WriteLine($"3.2) Object initialisiert by reference auf: {obj}");

            //4)
            using (StreamWriter sw = new StreamWriter("deleteme.txt"))
            {
                Console.WriteLine("4) in die Datei deleteme.txt werden folgende Zeilen geschrieben:");
                for (int j = 0; j < 3; j++)
                {
                    string zw = "Zeile " + j;
                    sw.WriteLine(zw);
                    Console.WriteLine(zw);
                }
                sw.Close();
            }

            using (StreamReader sr = new StreamReader("deleteme.txt"))
            {
                Console.WriteLine("Aus der Datei deleteme.txt werden folgende Zeilen gelesen:");
                string zr = sr.ReadLine();
                while(zr != null)
                {
                    Console.WriteLine(zr);
                    zr = sr.ReadLine();
                }
                sr.Close();
            }

        }

        public void f_ref(ref int refVar)
        {
            refVar = 23;
        }

        public void f_out(out Object refVar)
        {
            refVar = new object();
        }

    }
}
