﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x04_200_Nullables : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Eine int variable \"i\" deklarieren, die null werden kann und den Wert null an sie zuweisen.\n" +
            "- Video 0x04.1 12:00\n" +
            "\n" +
            "2) Eine normale (nicht nullable) int Variable \"j\" deklarieren und initializieren sie mit...\n" +
            "- mit dem Wert von i.CompareTo(5), falls die variable \"i\" nicht null ist.\n" +
            "- mit dem Wert 0, falls die Variabe \"i\" null ist.\n" +
            "- Video 0x04.1 14:20";

        public override void Execute()
        {
            // 1)
            int? i = null;
            if (i == null)
            {
                Console.WriteLine($"i --> null");
            }

            // 2)
            int j = i?.CompareTo(5) ?? 0;
            Console.WriteLine($"j --> {j}");

            
        }
    }
}
