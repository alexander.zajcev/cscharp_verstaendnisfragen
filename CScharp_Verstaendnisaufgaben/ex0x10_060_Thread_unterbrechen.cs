﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x10_060_Thread_unterbrechen : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Erstellen ein Thread, lassen ihn ein bisschen laufen\n" +
            "und unterbrechen ihn, bevor er sein Job gemacht hat.\n" +
            "- Video 0x0a 11:25 \n";

        public override void Execute()
        {
            My00060WorkerClass w1 = new My00060WorkerClass();
            Thread t1 = new Thread(w1.DoWork);
            t1.Start();

            Thread.Sleep(2000);
            w1.needStop = true;
            t1.Join();
        }
    }

    internal class My00060WorkerClass
    {
        public volatile bool needStop = false;
        public void DoWork()
        {
            while (true)
            {
                Thread.Sleep(30);
                Console.Write(".");
                if (needStop)
                {
                    Console.WriteLine();
                    break;
                }
            }
        }
    }
}
