﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x03_100_direct_cast_vs_as_cast : Verstaendnisaufgabe
    {

        public override string Aufgabenstellung { set; get; } = 
            "Demonstrieren, " +
            "wie unterschiedlich sich der direct cast \"(type)\" und der as-cast verhalten\n" +
            "- Unterschied beim nicht erfolgreichem Cast.\n" +
            "- Video: 0x03.1 vom Anfang";

        public override void Execute()
        {

            object o = new Object();
            try
            {
                String s1 = (String)o;
            }
            catch(Exception e)
            {
                Console.WriteLine($"Direct-Cast wirft eine Ausnahme bei nicht erfolgreichem Casten: {e.Message}");
            }

            String s2 = o as String;
            if (s2 is null)
            {
                Console.WriteLine("\nas-cast wirft keine Exception bei einem nicht erfolgrechem Cast, sondern gibt den null-Object als Ergebnis aus.");
            }

        }
    }
}
