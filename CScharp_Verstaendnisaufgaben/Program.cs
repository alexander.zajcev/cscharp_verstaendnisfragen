﻿namespace ZA_Verstaendnisaufgaben
{
    internal class Program
    {
        static void Main(string[] args)
        {
            List<Verstaendnisaufgabe> aufgaben = new List<Verstaendnisaufgabe>();

            aufgaben.Add(new ex0x02_100_Exceptions_Strings_usw());
            aufgaben.Add(new ex0x02_200_AbstractClasses_Interfaces());
            aufgaben.Add(new ex0x02_300_Classes_und_Structures());

            aufgaben.Add(new ex0x03_100_direct_cast_vs_as_cast());
            aufgaben.Add(new ex0x03_200_cast_boxing());
            aufgaben.Add(new ex0x03_300_operatoren());
            aufgaben.Add(new ex0x03_400_extension_methods());
            aufgaben.Add(new ex0x03_500_generische_typ_parameter());

            aufgaben.Add(new ex0x04_100_class_Type());
            aufgaben.Add(new ex0x04_200_Nullables());
            aufgaben.Add(new ex0x04_300_anonyme_classes());
            aufgaben.Add(new ex0x04_400_delegates_lambdas());

            aufgaben.Add(new ex0x05_100_ienumerable_yield());
            aufgaben.Add(new ex0x05_200_linq_erklaerung());
            aufgaben.Add(new ex0x05_300_linq_beispiele());

            //GUI-Aufgaben (0x06, 0x07, 0x08, 0x09) sind nicht klausurrelevant.

            aufgaben.Add(new ex0x10_050_Thread_creation());
            aufgaben.Add(new ex0x10_060_Thread_unterbrechen());
            aufgaben.Add(new ex0x10_100_Mutex());

            //TODO Aufgabe 0x0b (nicht klausurrelevant)

            foreach (Verstaendnisaufgabe v in aufgaben)
            {
                v.ExecuteWithBanner();
            }
        }
    }
}