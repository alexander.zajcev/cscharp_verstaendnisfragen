﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x05_300_linq_beispiele : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Vorbereitung: die drei Arrays deklarieren:\n" +
            "- string[] arrKategorien = new string[] { \"Obst\", \"Gemuese\" };\n" +
            "- string[] arrProdukte = new string[] { \"Apfel\", \"Gurke\", \"Orange\", \"Banane\", \"Grapefruit\", \"Moere\", \"Kolrabi\" };\n" +
            "- int[] arrZuordnung = new int[] { 1, 2, 1, 1, 1, 2, 2 };\n" +
            "\n" +
            "Mit hilfe von LINQ-Ausdrucken folgende Teilaufgaben erledigen:\n" +
            "1.1) In die Variable \"var kategorien\" anonyme objekte speichern, " +
            "in denen der Kategoriename aus \"arrKategorien\" zusammen mit Kategorie-ID gespeichert sind.\n" +
            "Die anonyme Objekte haben zwei Attribute: \"kategName\" und \"kategID\" (1 bei Obst und 2 bei Gemuese)\n" +
            "\n" +
            "1.2) In die Variable \"var produkte\" anonyme objekte speichern, " +
            "in denen der Produktname aus \"arrProdukte\" zusammen mit der entsprechenden Kategorie-ID aus \"arrZuordnung\" gespeichert sind.\n" +
            "Die anonyme Objekte haben zwei Attribute: \"prodName\" und \"kategID\"\n" +
            "- Video 0x005 25:45\n" +
            "\n" +
            "2) In zwei ineinanderliegenden from-abfragen die variablen \"kategorien\" und \"produkte\" " +
            "durchlaufen und nur die Produkte in die neue Variable \"var gleicheBuchstabe\" uebernehmen, " +
            "bei denen der entsprechende Gruppenname mit der gleichen Buchstabe wie der Produktname beginnt.\n" +
            "- Video 0x05 28:50\n" +
            "\n" +
            "3.1) Eintraege aus der Variable \"produkte\" nach der kategID sortieren. " +
            "Bei gleichen kategID sollen die Eintraege noch nach der ersten Buchstabe sortiert werden. \n" +
            "Die sortierten Eintraege In die Variable \"var orderedProductsLinq\" speichern.\n" +
            "\n" +
            "3.2) die gleiche Aufgabe wie in 3.1 soll nicht mit den LINQ-Ausdrucken geloest werden," +
            "sondern mithilfe von den im Linq-Packet enthaltenen IEnumerable-Extensions-Methods.\n" +
            "Ergebnis in der Variable \"var orderedProductsExtMethods\" speichern.\n" +
            "- Video 0x05 32:10\n" +
            "\n" +
            "4) Eintraege aus der Variable \"produkte\" gruppiern (nicht sortieren) nach der Laenge des Produktnamens.\n" +
            "Die entstandenen Gruppen, zusasammen mit der dazugehoerigen Laenge, in der Variable \"groupedProducts\" speichern.\n" +
            "- Video 0x05 34:00\n" +
            "\n" +
            "5) Einen LINQ-Ausdruck schreiben, der die Eintraege aus der Variable \"produkte\" " +
            "mit den Kategorien aus der \"kategorien\" anhand von \"kategID\" verknuepft.\n" +
            "Ergebnis in die Variable \"fullproducts\" speichern.\n" +
            "- Video 0x05 37:25\n" +
            "\n" +
            "6) Aus der Variable \"produkte\" fuer jede Kategorie jeweils einen (zufaelligen) Produkt-Eintrag auswaehlen " +
            "und in die Variable \"distinctProdukts\" speichern.\n" +
            "- Video 0x05 39:15";

        public override void Execute()
        {
            string[] arrKategorien = new string[] { "Obst", "Gemuese" };
            string[] arrProdukte = new string[] { "Apfel", "Gurke", "Orange", "Banane", "Grapefruit", "Moere", "Kolrabi" };
            int[] arrZuordnung = new int[] { 1, 2, 1, 1, 1, 2, 2 };

            // 1)
            var kategorien = from k in new int[] { 1, 2 }
                             select new { kategName = arrKategorien[k - 1], kategID = k };

            Console.WriteLine("1) kategorien:");
            foreach (var k in kategorien)
            {
                Console.WriteLine($"kategName:{k.kategName}, kategID: {k.kategID}");
            }


            var produkte = from p in new int[] { 0, 1, 2, 3, 4, 5, 6 }
                           select new { prodName = arrProdukte[p], kategID = arrZuordnung[p] };

            Console.WriteLine("\nprodukte:");
            foreach (var p in produkte)
            {
                Console.WriteLine($"prodName: {p.prodName}, kategID: {p.kategID}");
            }

            // 2)
            var gleicheBuchstabe = from k in kategorien
                                   from p in produkte
                                   where (k.kategID == p.kategID) && (k.kategName.ElementAt(0) == p.prodName.ElementAt(0))
                                   select k.kategName + ": " + p.prodName;

            Console.WriteLine("\n2)");
            foreach (string s in gleicheBuchstabe)
            {
                Console.WriteLine(s);
            }

            // 3.1)
            Console.WriteLine("\n3) Produkte sortiert nach der Kategorie und danach nach der ersten Buchstabe:");
            var orderedProductsLinq = from p in produkte
                                  orderby p.kategID, p.prodName.ElementAt(0)
                                  select $"kategID: {p.kategID}, prodName: {p.prodName}";

            Console.WriteLine("\n3.1)");
            foreach (string s in orderedProductsLinq)
            {
                Console.WriteLine(s);
            }

            // 3.2)
            var orderedProductsExtMethods = produkte.OrderBy(n => n.kategID).ThenBy(n => n.prodName.ElementAt(0));

            Console.WriteLine("\n3.2)");
            foreach (var p in orderedProductsExtMethods)
            {
                Console.WriteLine($"kategID: {p.kategID}, prodName: {p.prodName}");
            }

            // 4)
            var groupedProducts = from p in produkte
                                  group p by p.prodName.Length
                                  into g
                                  select new { length = g.Key, produkte = g };

            Console.Write("\n4) Produkte gruppiert bei der Wortlaenge:");
            foreach(var g in groupedProducts)
            {
                Console.WriteLine($"\nLength: {g.length}");
                foreach(var p in g.produkte)
                {
                    Console.WriteLine(p.prodName);
                }
            }

            // 5)
            var fullitems = 
                from p in produkte join k in kategorien
                on p.kategID equals k.kategID
                select $"{p.prodName} : {k.kategName}";

            Console.WriteLine($"\n5) Produkte joined mit Kategorien:");
            foreach (string s in fullitems)
            {
                Console.WriteLine(s);
            }

            // 6)
            var distinctProdukts = produkte.DistinctBy(p => p.kategID);

            Console.WriteLine("\n6) Produkte distinct by kategorie:");
            foreach(var p in distinctProdukts)
            {
                Console.WriteLine(p.prodName);
            }

        }

    }
}
