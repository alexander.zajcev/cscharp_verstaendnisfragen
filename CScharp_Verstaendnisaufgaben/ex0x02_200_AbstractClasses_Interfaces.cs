﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x02_200_AbstractClasses_Interfaces : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Interface IDrivable deklarieren mit einer einzelnen Methode \"void drive()\"\n" +
            "Classe BobbyCar schreiben, die das Interface IDrivable implementiert.\n" +
            "Eine abstrakte Classe Car schreiben, die das Interfce IDrivable implementiert " +
            "und die Methode \"drive()\" als abstract deklariert\n" +
            "Zwei Klassen \"BMW\" und \"Rover\" schreiben, die von der Klasse Car erben.\n" +
            "Im Test drei Variablen, je eine vom Typ BobbyCar, " +
            "BMW und Rover instantiiren und in eine Liste vom generic Typ IDrivable eintragen\n" +
            "Über die Einträge in der Liste iterieren und an jedem Eintrag die Methode drive() aufrufen.\n" +
            "- Video: 0x02.2 21:00";

        public override void Execute()
        {
            BobbyCar car1 = new BobbyCar();
            BMW car2 = new BMW();
            Rover car3 = new Rover();

            List<IDrivable> cars = new List<IDrivable> { car1, car2, car3 };
            foreach(IDrivable car in cars) { 
                car.drive(); 
            }
        }
    }

    public interface IDrivable
    {
        public void drive();
    }

    public class BobbyCar : IDrivable
    {
        public void drive()
        {
            Console.WriteLine("BobbiCar tu-tu-tu-u-u-u-u!");
        }
    }

    public abstract class Car : IDrivable
    {
        public string Marke { get; private set; }

        public Car(string marke)
        {
            Marke = marke;
        }

        public abstract void drive();
    }

    public class BMW : Car
    {
        public BMW() : base("BMW") {  }

        public override void drive()
        {
            Console.WriteLine(Marke + " faehrt.");
        }
    }

    public class Rover : Car
    {
        public Rover() : base("Rover") { }

        public override void drive()
        {
            Console.WriteLine(Marke + " is driving.");
        }
    }


}
