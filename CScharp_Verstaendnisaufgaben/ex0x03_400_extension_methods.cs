﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x03_400_extension_methods : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "Die Klasse \"string\" erweitern mit einer Methode \"string replaceNthCharacterWithX(int charNr)\", " +
            "die beim Aufrufen aus einer String-Variable ersetzt in der Variable die Buchstabe an der Position charNr durch " +
            "grosses X und gibt das Ergebnis zurueck.\n" +
            "- Video 0x03.1 25:20";

        public override void Execute()
        {
            Console.WriteLine($"\"abcdefgh\".replaceNthCharacterWithX(3) = {"abcdefgh".replaceNthCharacterWithX(3)}");
        }
    }

    //Erweiterungsmethoden mussen in irgendeinr statischen Klasse drin sein
    public static class Erweiterungsmethoden
    {
        public static  string replaceNthCharacterWithX(this string str, int charNr)
        {
            string ausgabe = str;
            if(str.Length > charNr)
            {
                ausgabe = str.Substring(0, charNr) + "X" + str.Substring(charNr + 1);
            }
            return ausgabe;
        }
    }

}
