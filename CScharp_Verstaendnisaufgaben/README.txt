Zusammenfassung zu den C#-Vorlesungen von Hr Voss. In den Aufgaben sind Verweise auf seine Videos. 
Und seine Code-Snippets kann man auch mit dem Namenspraefix zuordnen.

=== ex0x02_100_Exceptions_Strings_usw ==================================

Aufgabe:
1) Eine NullRefferenceException verusachen, sie fangen und die Fehlerbeschreibung ausgeben.
- Video 0x02.1 (von Anfang)
2) Eine decimalzahl mit string-interpolation ausgeben mit 4 Nachkommastellen links ausgerichtet auf breite 9
- Video 0x02.1 7min30
3.1) Eine Funktion f_ref implementieren, die einen int-Referenceparameter entgegennimmt und auf 23 setzt.Aus main mithilfe dieser Funktion eine int-Variable aendern.
3.2) Eine Funktion f_out implementieren, die einen nicht initialisierten (null) Object-Parameter initialisiert.
- Video 0x02.1 13:45
4) In eine Textdatei "deleteme.txt" drei Zeilen Text mithilfe von StreamWriter schreiben.
Danach die drei Zeilen aus der Textdatei mithilfe von StreamReader auslesen und ausgeben.
- Video 0x2.1 20:00

--- Executing ---

1) Exception gefangen: Object reference not set to an instance of an object.
2) Zahl 12.3456789: '12.3457  '
3.1) Zahl geaendert by reference von 22 auf: 23 
3.2) Object initialisiert by reference auf: System.Object
4) in die Datei deleteme.txt werden folgende Zeilen geschrieben:
Zeile 0
Zeile 1
Zeile 2
Aus der Datei deleteme.txt werden folgende Zeilen gelesen:
Zeile 0
Zeile 1
Zeile 2

=== The End ===



=== ex0x02_200_AbstractClasses_Interfaces ==================================

Aufgabe:
Interface IDrivable deklarieren mit einer einzelnen Methode "void drive()"
Classe BobbyCar schreiben, die das Interface IDrivable implementiert.
Eine abstrakte Classe Car schreiben, die das Interfce IDrivable implementiert und die Methode "drive()" als abstract deklariert
Zwei Klassen "BMW" und "Rover" schreiben, die von der Klasse Car erben.
Im Test drei Variablen, je eine vom Typ BobbyCar, BMW und Rover instantiiren und in eine Liste vom generic Typ IDrivable eintragen
Über die Einträge in der Liste iterieren und an jedem Eintrag die Methode drive() aufrufen.
- Video: 0x02.2 21:00

--- Executing ---

BobbiCar tu-tu-tu-u-u-u-u!
BMW faehrt.
Rover is driving.

=== The End ===



=== ex0x02_300_Classes_und_Structures ==================================

Aufgabe:
Demonstrieren, wie unterschiedlich sich der Reference-Typ (Class) und der Value-Typ (Structure) verhalten
a) Beim Zuweisen einer Variable des Typs an eine andere.
b) Wenn man die Typen beim Aufruf von Funktionen als Parameter uebergibt.
- Video: 0x02.2 25:35

--- Executing ---

pc01: x=1000; y=2000 [class]
pc02: x=1000; y=2000 [class]
pc02 und pc01 Variablen sind Aliase auf dieselbe Instanz.

ps01: x=30; y=40 [struct]
ps02: x=3000; y=4000 [struct]
ps02 und ps01 Variablen bezeichnen unterschiedliche Instanzen.

pc01: x=1001; y=2001 [class]
ps01: x=30; y=40 [struct]
Class-Parameters werden an die Funktionen by Reference uebergeben und die Struct-Parameters by Value.

=== The End ===



=== ex0x03_100_direct_cast_vs_as_cast ==================================

Aufgabe:
Demonstrieren, wie unterschiedlich sich der direct cast "(type)" und der as-cast verhalten
- Unterschied beim nicht erfolgreichem Cast.
- Video: 0x03.1 vom Anfang

--- Executing ---

Direct-Cast wirft eine Ausnahme bei nicht erfolgreichem Casten: Unable to cast object of type 'System.Object' to type 'System.String'.

as-cast wirft keine Exception bei einem nicht erfolgrechem Cast, sondern gibt den null-Object als Ergebnis aus.

=== The End ===



=== ex0x03_200_cast_boxing ==================================

Aufgabe:
Demonstrieren, wie passiert Boxing beim Casting von einem Value-Type (z.B. Struct) zu einem Reference-Type (z.B. Interface).
- Video 0x03.1 min 04:30

--- Executing ---

Beim Zuweisen eines Struct-Object zu einer Interface-Variable wird der Struct-Object geklonnt und in einen Reference-Typ umgewandelt (Boxing):
objSampleStruct.Name = Klaus
obj1_I_Namable.Name = Klaus

Als Bestaetigung aendern wir eine der beiden Variablen und sehen, dass die andere unveraendert bleibt:

objSampleStruct.Name = Klaus
obj1_I_Namable.Name = Hans


Um zu zeigen, dass beim Boxing der Value-Typ (Struct) in einen Reference-Typ gepackt wurde weise ich einer anderen Interface-Variable die erste Interface-Variable zu:

obj1_I_Namable.Name = Hans
obj2_I_Namable.Name = Hans

Jetzt beim Aendern einer der beiden Variablen die zweite aendert sich auch, was bestaetigt, dass die beide Variablen auf die selbe Instanz zeigen (Reference-Type):

obj2_I_Namable.Name = Ralf
obj1_I_Namable.Name = Ralf


=== The End ===



=== ex0x03_300_operatoren ==================================

Aufgabe:
1) Implementieren eine Klasse MyCast mit einem Integer-Property intProp0. In dieser Klasse implementieren zwei explicite Cast-Operatoren, einen von integer zu MyCast und einen von MyCast zu integer
- Video 0x03.1 08:50

2) In der Klasse MyCast implementieren den "+"-Operator mit dem man einen MyCast-Object mit einem anderen MyCast-Object oder mit ener int-Variable addieren kann.
- Video 0x03.1 16:50

3) In die Klasse MyCast hinzufuegen noch ein privates Integer-Property (intProp1).
Implementieren einen Indexer-Operator ([]), der fuer den index [0] das Property intProp0 setzt/zurueckgibt
und fuer alle anderen Indexe setzt/zurueckgibt das Property intProp1
- Video 0x03.1 23:00


--- Executing ---

1.1) MyCast mc = (MyCast)15; --> mc = [MyCast Instanz mit intProp0=15]
1.2) int i = (int)mc; --> i = 15
2.1) mc = mc + 5; --> mc = [MyCast Instanz mit intProp0=20]
2.2) MyCast mc2 = 10 + mc; --> mc2 = [MyCast Instanz mit intProp0=30]
2.3) mc + mc2 = --> [MyCast Instanz mit intProp0=50]
3) mc[0] = --> 20
   mc[55] = 99; --> mc[55] = 99

=== The End ===



=== ex0x03_400_extension_methods ==================================

Aufgabe:
Die Klasse "string" erweitern mit einer Methode "string replaceNthCharacterWithX(int charNr)", die beim Aufrufen aus einer String-Variable ersetzt in der Variable die Buchstabe an der Position charNr durch grosses X und gibt das Ergebnis zurueck.
- Video 0x03.1 25:20

--- Executing ---

"abcdefgh".replaceNthCharacterWithX(3) = abcXefgh

=== The End ===



=== ex0x03_500_generische_typ_parameter ==================================

Aufgabe:
1) Eine Klasse "GenerischerParameter" implementieren, die ein einzelnes Attribut vom Generischen Typ hat.
Den Konstruktor implementieren, der einen einzelnen Parameter vom Generischen Typ nimmt und das Attribut setzt.
Ueberschreiben die ToString Methode so, dass sie den generischen Attribut ausgibt.
- Video 0x03.1 31:00

2) Den Typ des generischen Parameters beschraenken so, dass nur diejenigen Typ-Parameter erlaubt sind, die das Interface IConvertible implementieren.
eine Methode "double add5komma5()" zu der Klasse hinzufuegen, die zu dem Attribut die Zahl 5.5 addiert und das Ergebnis ausgibt.

--- Executing ---

1) new GenerischerParameter<string>("foo bar") = der attribut hat den Wert: foo bar
2) new GenerischerParameter<Double>(2.1).add5komma5() = 7.6
   new GenerischerParameter<Boolean>(true).add5komma5() = 6.5

=== The End ===



=== ex0x04_100_class_Type ==================================

Aufgabe:
1) Eine Variable i vom Typ "int" deklarieren. Die Typinformationen der Variable "i" in einer Variable "t" speichern. Herausfinden den wirklichen Classennamn und den Namen der Basisklasse.
- Video 0x04.1 vom Anfang

2) Alle Interfaces auflisten, die in dem Typ "int" implementiert sind.
- Video 0x04.1 05:10

3) Ueber die Variable "t" die Methode "CompareTo(int value)" der Variable i aufrufen mit dem Parameter 5.
- Video 0x4.1 09:10


--- Executing ---

t.Name --> Int32
t.BaseType.Name --> ValueType

2) Interfaces: 
IComparable
IConvertible
ISpanFormattable
IFormattable
IComparable`1
IEquatable`1
IBinaryInteger`1
IBinaryNumber`1
IBitwiseOperators`3
INumber`1
IAdditionOperators`3
IAdditiveIdentity`2
IComparisonOperators`2
IEqualityOperators`2
IDecrementOperators`1
IDivisionOperators`3
IIncrementOperators`1
IModulusOperators`3
IMultiplicativeIdentity`2
IMultiplyOperators`3
ISpanParseable`1
IParseable`1
ISubtractionOperators`3
IUnaryNegationOperators`2
IUnaryPlusOperators`2
IShiftOperators`2
IMinMaxValue`1
ISignedNumber`1

3) i.CompareTo(5) wird aufgerufen. Ergebnis: -1

=== The End ===



=== ex0x04_200_Nullables ==================================

Aufgabe:
1) Eine int variable "i" deklarieren, die null werden kann und den Wert null an sie zuweisen.
- Video 0x04.1 12:00

2) Eine normale (nicht nullable) int Variable "j" deklarieren und initializieren sie mit...
- mit dem Wert von i.CompareTo(5), falls die variable "i" nicht null ist.
- mit dem Wert 0, falls die Variabe "i" null ist.
- Video 0x04.1 14:20

--- Executing ---

i --> null
j --> 0

=== The End ===



=== ex0x04_300_anonyme_classes ==================================

Aufgabe:
1) eine Variable "p1" des variablen Typs deklarieren. 
Initialisieren sie mit einer Instanz eines anonymen Typs, der Zwei Attribute hat: ein int-Attribut mit dem Namen intAttr und ein string-Attribut mit dem Namen strAttr.
das int-Attribut bei der Initialisierung auf 3 setzen, und das string-Attribut auf "Hallo".
- Video 0x04.1 17:00

2) Eine int-Variable mit dem Namen intVar deklarieren und mit dem Wert 5 initialisieren.
Eine string-Variable mit dem Namen strVar deklarieren und mit dem Wert "Salut" initialisieren.
eine Variable "p2" des variablen Typs deklarieren und mit Instanz eines anonymen Typs initialisieren.
der anonyme Typ soll zwei Attribute aus den Variablen intVar und strVar bekommen.


--- Executing ---

p1.intAttr --> 3, p1.strAttr --> Hallo
p2.intVar --> 5, p2.strVar --> Salut

=== The End ===



=== ex0x04_400_delegates_lambdas ==================================

Aufgabe:
1) Mit Hilfe eines Lambda-Ausdrucks eine Funktion mit dem namen "teileAdurchB" erstellen. Die Fuktion soll zwei double-Parameter nehmen, den ersten Parameter durch den zweiten teilen und das Ergebnis zurueckgeben.

2) Einen Typ mit dem Namen "MathFunction" erstellen, der Referenzen auf Funktionen repraesentiert, die einen double-Parameter nehmen und einen double-Wert zurueckgeben.
- Eine Variable "f" des Typs MathFunction deklarieren und mit der Referenz auf die Funktion Math.Floor initialisieren.
Ueber die Variable f die refernzierte Funktion mit dem Parameter 5.4 aufrufen und den Rueckgabewert ausgeben.
- Der Variable f eine Referenz auf die Funktion Math.Ceiling zuweisen.
Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.
- Video 0x04.2 vom Anfang

3)Der Variable f eine Referenz auf eine anonyme Funktion zuweisen, die den uebergebenen double-Parameter mit 2 multipliziert und das Ergebnis zurueckgibt.
Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.
-Video 0x04.2 07:00

4) mithilfe eines Lambda-Ausdrucks zuweisen der Variable f eine Referenz auf eine anonyme Funktion, die den uebergebenen double-Parameter durch 2 teilt und das Ergebnis zurueckgibt.
Wieder die von f referenzierte funktion mit dem Parameter 5.4 aufrufen.
- Video 0x04.2 07:50

5.1) Eine Variable mit dem Namen "a" eines Anonymous Typs deklarieren.
Ter Typ soll Referenzen auf Funktionen representieren, zwei Parameter entgegennehmen: der erste vom Typ "double", der zweite vom typ "int". Und einen double Ergebnis zurueckgeben.
Der Variable "a" eine Referenz auf eine anonyme Lambda-Funktion zuweisen. Die anonyme Funktion soll den ersten int-Parameter durch den zweiten Teilen und das Ergebnis zuruckegeben.
Ueber die Variable a die refernzierte Funktion mit den Parameter 5 und 3 aufrufen und den Rueckgabewert ausgeben.

5.2) Eine Variable mit dem Namen "v" eines Anonymous Typs deklarieren.
Ter Typ soll Referenzen auf void-Funktionen representieren, die zwei string-Parameter entgegennehmen.
Der Variable "v" eine Referenz auf eine anonyme Lambda-Funktion zuweisen. Die anonyme Funktion soll die zwei Strings, getrennt durch ein Leerzeiche auf der Console ausgeben.
Ueber die Variable "v" die refernzierte Funktion mit den Parameter "hello" und "world" aufrufen.
- Video 0x04.2 10:45

--- Executing ---

1) teileAdurchB(5, 2) --> 2.5

2)
Floor: f(5.4) --> 5
Ceiling: f(5.4) --> 6

3) <Execute>b__4_0: f(5.4) --> 10.8

4) <Execute>b__4_1: f(5.4) --> 2.7

5.1) <Execute>b__4_2: a(5, 3) --> 1.6666666666666667

5.2) <Execute>b__4_3: v("hello", "world") --> 

hello world

=== The End ===



=== ex0x05_100_ienumerable_yield ==================================

Aufgabe:
1) Eine Klasse mit dem Namen "ThreeValuesCollection" implementieren. Die Klasse soll einen Konstruktor mit drei int-Parametern besitzen.
Die Klasse soll den Interface "IEnumerable" implementieren so, dass Instanzen der Klasse in der foreach-Schleife verwendet werden koennen.
Beim Durchlaufen der foreach-Schleife sollen die drei im Konstruktor entgegengenommene int-Parametern nacheinander ausgegeben werden
Um "IEnumerable" zu implementieren, ist noch eine Klasse erforderlich, die den "IEnumerator" Interface implementiert. Die Klasse soll "ThreeValuesEnumerator" heissen.

2) Eine Funktion mit dem Namen "threeValuesFunktion" schreiben, die genau dieselben Anforderungen wie im Punkt 1) erfuellt:
- funktion nimmt 3 int-Parametern entgegen
- der Rueckgabetyp der Funktion ist IEnumerable
- Beim durchlaufen des Rueckgabewertes der Funktion in der foreach-Schleife sollen nacheinander die in den Parameter uebergebene Zahlen ausgegeben werden.
- Video 0x05.1 vom Anfang

--- Executing ---

1) ThreeValuesCollection(5, 7, 9), foreach-Schleife: -->

naechster Element: 5
naechster Element: 7
naechster Element: 9

2) foreach(int i in threeValuesFunktion(100, 200, 300)) -->

naechster Element: 100
naechster Element: 200
naechster Element: 300

=== The End ===



=== ex0x05_200_linq_erklaerung ==================================

Aufgabe:
Als Vorbereitung drei ganz normale Funktionen implementieren:
- int getStringLength(string s) : gibt die Laenge des Strings zurueck.
- bool obWirDasWortBrauchenOderNicht(string s) : gibt zurueck, ob die Laenge des Strings groesser als 4 ist
- string stringToUpperCase(string s) : gibt den String mit allen Grossbuchstaben zurueck.
- string-Array mit dem Namen "strOrig" und zehn Woerter initialisieren: eins,zwei,drei,vier,fuenf,sechs,sieben,acht,neun,zehn

Mithilfe der vom LINQ bereitgestellten Extension-Methoden das Array "strOrig" wie folgt transformieren:
1.1) Sortieren absteigend nach der Wortlaenge. Ergebnis in der Variable "strOrder" speichern.
1.2) Filtern die in strOrder gespeicherten Woerter so, dass nur die Woerter mit der Laenge groesser 4 bleiben.Ergebnis speichern in der Variable "strWhere".
1.3) Alle in strWhere gespeicherten Woerter zu Grossbuchstaben umwandeln und in der Variabel "strSelect" speichern.

2) Dieselben Transformationen an dem strOrig mit einem einzigen LINQ-Ausdruck durchfuehren und in der Variable "linqResult" speichern.

- Video von 13:10 bis 25:45

--- Executing ---

1.1) sortiert nach Laenge:
sieben
fuenf
sechs
eins
zwei
drei
vier
acht
neun
zehn
1.2) Laenge 4 ausgefiltert:
sieben
fuenf
sechs
1.3) Zu Grossbuchstaben konvertiert:
SIEBEN
FUENF
SECHS

2) Sortiert, filtered und zu Grossbuchstaben konvertiert mit einem LINQ-Ausdruck:
SIEBEN
FUENF
SECHS

=== The End ===



=== ex0x05_300_linq_beispiele ==================================

Aufgabe:
Vorbereitung: die drei Arrays deklarieren:
- string[] arrKategorien = new string[] { "Obst", "Gemuese" };
- string[] arrProdukte = new string[] { "Apfel", "Gurke", "Orange", "Banane", "Grapefruit", "Moere", "Kolrabi" };
- int[] arrZuordnung = new int[] { 1, 2, 1, 1, 1, 2, 2 };

Mit hilfe von LINQ-Ausdrucken folgende Teilaufgaben erledigen:
1.1) In die Variable "var kategorien" anonyme objekte speichern, in denen der Kategoriename aus "arrKategorien" zusammen mit Kategorie-ID gespeichert sind.
Die anonyme Objekte haben zwei Attribute: "kategName" und "kategID" (1 bei Obst und 2 bei Gemuese)

1.2) In die Variable "var produkte" anonyme objekte speichern, in denen der Produktname aus "arrProdukte" zusammen mit der entsprechenden Kategorie-ID aus "arrZuordnung" gespeichert sind.
Die anonyme Objekte haben zwei Attribute: "prodName" und "kategID"
- Video 0x005 25:45

2) In zwei ineinanderliegenden from-abfragen die variablen "kategorien" und "produkte" durchlaufen und nur die Produkte in die neue Variable "var gleicheBuchstabe" uebernehmen, bei denen der entsprechende Gruppenname mit der gleichen Buchstabe wie der Produktname beginnt.
- Video 0x05 28:50

3.1) Eintraege aus der Variable "produkte" nach der kategID sortieren. Bei gleichen kategID sollen die Eintraege noch nach der ersten Buchstabe sortiert werden. 
Die sortierten Eintraege In die Variable "var orderedProductsLinq" speichern.

3.2) die gleiche Aufgabe wie in 3.1 soll nicht mit den LINQ-Ausdrucken geloest werden,sondern mithilfe von den im Linq-Packet enthaltenen IEnumerable-Extensions-Methods.
Ergebnis in der Variable "var orderedProductsExtMethods" speichern.
- Video 0x05 32:10

4) Eintraege aus der Variable "produkte" gruppiern (nicht sortieren) nach der Laenge des Produktnamens.
Die entstandenen Gruppen, zusasammen mit der dazugehoerigen Laenge, in der Variable "groupedProducts" speichern.
- Video 0x05 34:00

5) Einen LINQ-Ausdruck schreiben, der die Eintraege aus der Variable "produkte" mit den Kategorien aus der "kategorien" anhand von "kategID" verknuepft.
Ergebnis in die Variable "fullproducts" speichern.
- Video 0x05 37:25

6) Aus der Variable "produkte" fuer jede Kategorie jeweils einen (zufaelligen) Produkt-Eintrag auswaehlen und in die Variable "distinctProdukts" speichern.
- Video 0x05 39:15

--- Executing ---

1) kategorien:
kategName:Obst, kategID: 1
kategName:Gemuese, kategID: 2

produkte:
prodName: Apfel, kategID: 1
prodName: Gurke, kategID: 2
prodName: Orange, kategID: 1
prodName: Banane, kategID: 1
prodName: Grapefruit, kategID: 1
prodName: Moere, kategID: 2
prodName: Kolrabi, kategID: 2

2)
Obst: Orange
Gemuese: Gurke

3) Produkte sortiert nach der Kategorie und danach nach der ersten Buchstabe:

3.1)
kategID: 1, prodName: Apfel
kategID: 1, prodName: Banane
kategID: 1, prodName: Grapefruit
kategID: 1, prodName: Orange
kategID: 2, prodName: Gurke
kategID: 2, prodName: Kolrabi
kategID: 2, prodName: Moere

3.2)
kategID: 1, prodName: Apfel
kategID: 1, prodName: Banane
kategID: 1, prodName: Grapefruit
kategID: 1, prodName: Orange
kategID: 2, prodName: Gurke
kategID: 2, prodName: Kolrabi
kategID: 2, prodName: Moere

4) Produkte gruppiert bei der Wortlaenge:
Length: 5
Apfel
Gurke
Moere

Length: 6
Orange
Banane

Length: 10
Grapefruit

Length: 7
Kolrabi

5) Produkte joined mit Kategorien:
Apfel : Obst
Gurke : Gemuese
Orange : Obst
Banane : Obst
Grapefruit : Obst
Moere : Gemuese
Kolrabi : Gemuese

6) Produkte distinct by kategorie:
Apfel
Gurke

=== The End ===



=== ex0x10_050_Thread_creation ==================================

Aufgabe:
Create one Thread, start it and wait before it complets it's job.
- Video 0x0a (von Anfang)
- Threads mit Parameter: Video 17:30

--- Executing ---

Der Thread t1.DoWork
Der Thread pt1.DoWorkWithParams intParam: 33433; strParam: Hallo

=== The End ===



=== ex0x10_060_Thread_unterbrechen ==================================

Aufgabe:
Erstellen ein Thread, lassen ihn ein bisschen laufen
und unterbrechen ihn, bevor er sein Job gemacht hat.
- Video 0x0a 11:25 


--- Executing ---

............................................................

=== The End ===



=== ex0x10_100_Mutex ==================================

Aufgabe:
Zwei Threads ändern gleichzeitig denselben globalen counter.
Jeder Thread macht insgesamt dieselbe Anzahl von Änderungen.
Ein Thread inkrementiert um 1 und der andere dekrementiert um 1.
Aufgabe: Threadsafety gewaehrleisten mit dem Mutex so, 
dass am ende der Counter so groß ist wie am Anfang, und zwar 0.
- Video 0x0a ab 19:40
- code: 0x0a B_Mutex


--- Executing ---

Results: counter1 = -114775, counter2 = -184207


=== The End ===



