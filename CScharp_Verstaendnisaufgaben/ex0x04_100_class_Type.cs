﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ZA_Verstaendnisaufgaben
{
    internal class ex0x04_100_class_Type : Verstaendnisaufgabe
    {
        public override string Aufgabenstellung { get; set; } =
            "1) Eine Variable i vom Typ \"int\" deklarieren. Die Typinformationen der Variable \"i\" in einer Variable \"t\" speichern. " +
            "Herausfinden den wirklichen Classennamn und den Namen der Basisklasse.\n" +
            "- Video 0x04.1 vom Anfang\n" +
            "\n" +
            "2) Alle Interfaces auflisten, die in dem Typ \"int\" implementiert sind.\n" +
            "- Video 0x04.1 05:10\n" +
            "\n" +
            "3) Ueber die Variable \"t\" die Methode \"CompareTo(int value)\" der Variable i aufrufen mit dem Parameter 5.\n" +
            "- Video 0x4.1 09:10\n" +
            "";

        public override void Execute()
        {
            // 1)
            int i = 2;
            Type t = i.GetType();
            Console.WriteLine($"t.Name --> {t.Name}");
            Console.WriteLine($"t.BaseType.Name --> {t.BaseType.Name}\n");


            // 2)
            Console.WriteLine("2) Interfaces: ");
            foreach(var ifc in i.GetType().GetInterfaces())
            {
                Console.WriteLine(ifc.Name);
            }

            // 3)
            MethodInfo mi = t.GetMethod("CompareTo", new Type[] { t });
            Console.WriteLine($"\n3) i.CompareTo(5) wird aufgerufen. Ergebnis: {mi.Invoke(i, new object[] { 5 })}");
            
        }
    }
    
}
